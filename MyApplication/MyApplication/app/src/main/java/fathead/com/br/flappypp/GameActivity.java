package fathead.com.br.flappypp;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.View;

import fathead.com.br.flappypp.model.Constants;
import fathead.com.br.flappypp.model.Hero;
import fathead.com.br.flappypp.model.Pipe;
import fathead.com.br.flappypp.model.Sprite;
import fathead.com.br.flappypp.util.Utils;
/**
 * Created by leonardo on 01/06/16.
 * Classe responsavel pela tela do jogo.
 */
public class GameActivity extends Activity implements View.OnTouchListener {
    //Custom view que desenha tela
    MySurfaceView surfaceView;
    //Classe que contem informacoes do personagem
    Hero hero;
    //Contralam os eixos dos bitmaps desenhados na e o xPipes serve para fazer a movimentacao do canos
    float x, y, xPipes;
    //Variavel de pontuacao
    int score = 0;
    //Intent para a tela de Game Over
    Intent gameOver;
    //Sprite classe que faz a tratativa de sprites
    Sprite sprite;
    //Musica do jogo
    MediaPlayer music;
    //Som para o game over
    MediaPlayer gameOverSound;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Intanciando a custom view e adicionando o evento de click
        surfaceView = new MySurfaceView(this);
        surfaceView.setOnTouchListener(this);
        //Intanciando um novo Hero com um Bitmap
        hero = new Hero(BitmapFactory.decodeResource(getResources(), R.drawable.moisesconsegue));
        //Posicao do hero em x
        x = 250;
        //Posicao do hero em y;
        y = 0;
        //Posicao inicial do cano
        xPipes = 950;
        //Criando a matriz de canos
        Utils.createPipesStatic(this);
        //Ultilizando a custom view como layout.
        setContentView(surfaceView);
        //Criando a musica do jogo
        music = MediaPlayer.create(this, R.raw.loop);
        //Criando o som de game over
        gameOverSound = MediaPlayer.create(this, R.raw.gameover);
        //Criando o intent de para tela de game over
        gameOver = new Intent(this, GameOverActivity.class);


    }

    //Estado da activity.
    @Override
    protected void onPause() {
        super.onPause();
        surfaceView.pause();
    }

    //Estado da activity.
    @Override
    protected void onResume() {
        super.onResume();
        //Iniciando a musica
        music.start();
        surfaceView.resume();
    }

    //Estado da activity.
    @Override
    protected void onStop() {
        super.onStop();
        music.stop();
    }

    //Responsavel por capturar eventos o Touch
    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {

        switch (motionEvent.getAction()) {
            case MotionEvent.ACTION_DOWN:
                //Encrentando a velocidade em Y do sprite
                sprite.setYSpeed(-30);
                break;
            case MotionEvent.ACTION_UP:
                //Encrentando a velocidade em Y do sprite
                sprite.setYSpeed(20);
                break;
            default:
                break;
        }
        return true;
    }

    /**
     * Classe que e responsavel por desenhar na tela e fazer as tratativas de colisao
     */
    public class MySurfaceView extends android.view.SurfaceView implements Runnable {
        //Possibilitar o procesamento em paralelo com a Main Thread
        Thread thread;
        //Monitora mudancas na Surface
        SurfaceHolder holder;

        //Construtor que recebe o contexto atual
        public MySurfaceView(Context context) {
            super(context);
            holder = getHolder();
        }


        public void run() {
            //Instacia a classe Sprite, passando a Surface e o Hero
            sprite = new Sprite(MySurfaceView.this, hero);
            //Game loop
            while (Constants.isRun) {
                //Avaliando se a surface esta disponivel
                if (!holder.getSurface().isValid()) {
                    continue;
                }
                //Incrementa o y, para a movimentacao do hero
                y += 10;
                //Altera as posicoes do hero
                hero.positionX = x;
                hero.positionY = y;
                //Instacia de canvas da surface
                Canvas canvas = holder.lockCanvas();

                Draw(canvas);
                //Finaliza a edicao de pixels e faz a chamada do lockCanvas()
                holder.unlockCanvasAndPost(canvas);
            }
        }

        //Faz a interrupcao da thread.
        public void pause() {
            Constants.isRun = false;
            try {
                thread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            thread = null;
        }

        //Faz o start da Thread
        public void resume() {
            Constants.isRun = true;
            //start thread to invoke the run
            thread = new Thread(this);
            thread.start();
        }

        //Responsavel por fazer os desenhos na tela
        protected void Draw(Canvas canvas){
            //Desenha o backgroud
            canvas.drawARGB(255, 0, 153, 204);
            //Chamdo do metodo que desenha os sprotes do hero
            sprite.onDraw(canvas);

            //Desenha piso
            myDrawRect(new Rect(0, (canvas.getHeight() * 95) / 100, canvas.getWidth(), canvas.getHeight()), R.color.brown, canvas);

            //Desenha os canos
            for (int i = 0; i < 1; i++) {
                for (int j = 0; j < 2; j++) {

                    canvas.drawBitmap(Constants.pipes[i][j].getImage(), Constants.pipes[i][j].getX(), Constants.pipes[i][j].getStartY(), null);
                    //Setando dinamicamente X
                    setXInPipes(Constants.pipes[i][j]);

                    //Checa se houve pontuacao
                    if (j == 0) {
                        if (checkScore(x, Constants.pipes[i][j])) {
                            score += 1;
                        }
                    }
                }
            }

            //Desenha a pontuacao
            myDrawText(canvas, score / 40);

        }

        //Desenha um retangulo
        void myDrawRect(Rect rect, int colorResource, Canvas canvas) {
            Paint paintAtt = new Paint();
            int color = getResources().getColor(colorResource);
            paintAtt.setColor(color);
            paintAtt.setStyle(Paint.Style.FILL);
            canvas.drawRect(rect, paintAtt);
        }

        //Escreve na tela
        void myDrawText(Canvas canvas, int score) {
            Paint paintAtt = new Paint();
            paintAtt.setColor(Color.BLACK);
            paintAtt.setTextSize(80);
            canvas.drawText("" + score, canvas.getWidth() / 2, 60, paintAtt);
        }

        //Movimentacao dos canos em X
        void setXInPipes(Pipe pipe) {
            pipe.setX(pipe.getX() - 10);
            if (pipe.getX() < (-100)) {
                pipe.resetPositionStart(pipe.getStartX());
            }
        }

        //Verifica se houve uma pontuacao, checando o X do cano e o do hero
        boolean checkScore(float x, Pipe pipe) {
            boolean isScore = false;
            if (x > pipe.getX()) {
                isScore = true;
            }
            return isScore;
        }

        //Faz a transicao de tela e toca o som de Game Over
        public void gameOver(){
            gameOverSound.start();
            gameOver.putExtra("Score", score);
            startActivity(gameOver);
            finish();
        }
    }
}
