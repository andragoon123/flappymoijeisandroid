package fathead.com.br.flappypp.model;

import android.content.Context;
import android.media.MediaPlayer;
import android.os.Handler;

import fathead.com.br.flappypp.R;
import fathead.com.br.flappypp.model.Constants;

public class AudioManager extends Handler implements Runnable{
    Context currentContex;
    MediaPlayer music, gameOver;

    public AudioManager(Context context){
        currentContex = context;
        music = MediaPlayer.create(currentContex, R.raw.loop);
        gameOver = MediaPlayer.create(currentContex, R.raw.gameover);
    }

    public void stop(){
        removeCallbacksAndMessages(null);
    }

    @Override
    public void run() {

        MediaPlayer music = MediaPlayer.create(currentContex, R.raw.loop);

        while (Constants.isRun){
            if(Constants.stateMusic.equals("ON")){
                music.start();
            }
        }
        music.stop();
        gameOver.start();
        removeCallbacks(this);

    }
}