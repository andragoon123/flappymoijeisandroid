package fathead.com.br.flappypp;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import fathead.com.br.flappypp.model.Constants;
import fathead.com.br.flappypp.model.Items;

/**
 * Created by leonardo on 02/06/16.
 * Tela de Game Over, responsavel pelo reinicio e por salvar pontuação.
 */
public class GameOverActivity extends Activity implements View.OnClickListener{
    //Bundle classe responsavel por obter dados vindo do intent.
    Bundle extras;
    // Variave responsavel por amarzenar a pontuacao
    int score;
    //View que cria um campo de digitacao para o usuario
    EditText name;
    //Responsavel por fazer a persistencia das informacoes para a criacao do ranking
    SharedPreferences preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_over);
        //Instacia do espaco de armazenamento de informacoes para o ranking
        preferences = getSharedPreferences("ranking", MODE_PRIVATE);
        //Pegando informacao vinda do intent
        extras = getIntent().getExtras();
        //Ultilizando extras para obter o score
        score = extras.getInt("Score", 0);
        //Declaracao da view utilizando o id
        name = (EditText) findViewById(R.id.insert_name);
        //Declaracao da view utilizando o id
        ((TextView) findViewById(R.id.score)).setText(score/40+"");
        //Setando o evento de click
        ((Button) findViewById(R.id.retry_button)).setOnClickListener(this);
        //Setando o evento de click
        ((Button) findViewById(R.id.ranking_button_game_over)).setOnClickListener(this);
        //Setando a visibiliada da view
        ((Button) findViewById(R.id.ranking_button_game_over)).setVisibility(View.GONE);
        //Setando o evento de click
        ((Button) findViewById(R.id.button_submit)).setOnClickListener(this);


    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.retry_button:
                //Fazendo a transicao de activity
                GameOverActivity.this.startActivity(new Intent(GameOverActivity.this, GameActivity.class));
                finish();
                break;
            case R.id.ranking_button_game_over:
                //Fazendo a transicao de activity
                GameOverActivity.this.startActivity(new Intent(GameOverActivity.this, RankingActivity.class));
                finish();
                break;
            case R.id.button_submit:
                //Pegando a informacao do edit text
                String nameS = String.valueOf(name.getText());
                //Modificando a visibilidade do botao game over
                ((Button) findViewById(R.id.ranking_button_game_over)).setVisibility(View.VISIBLE);
                //Modificando a visibilidade do botao submit
                ((Button) findViewById(R.id.button_submit)).setVisibility(View.GONE);
                //Modificando a visibilidade do campo name
                name.setVisibility(View.GONE);
                //Adicionando um item a lista
                Constants.itemsList.add(new Items(nameS, ""+score));
                savePreferences();
                break;
            default:
                break;
        }
    }

    //Fazendo a persistencias dos dados da lista.
    void savePreferences(){
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt("Status_size", Constants.itemsList.size());

        for(int i=0; i < Constants.itemsList.size();i++) {
            editor.remove("Name_" + i);
            editor.putString("Name_" + i, Constants.itemsList.get(i).getName());

            editor.remove("Score_" + i);
            editor.putString("Score_" + i, Constants.itemsList.get(i).getScore());
        }

        editor.commit();
    }
}
