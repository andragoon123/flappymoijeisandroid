package fathead.com.br.flappypp.model;

/**
 * Created by leonardo on 10/06/16.
 */
public class Items {
    private String name;
    private String score;

    public Items(String name, String score){
        this.name = name;
        this.score = score;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getScore() {
        return score;
    }

    public void setScore(String score) {
        this.score = score;
    }
}
