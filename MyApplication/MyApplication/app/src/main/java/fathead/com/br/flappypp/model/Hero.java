package fathead.com.br.flappypp.model;

import android.graphics.Bitmap;

/**
 * Created by leonardo on 08/06/16.
 */
public class Hero {

    private Bitmap bitmap;

    public float width, height;
    public float positionX;
    public float positionY;

    public Hero(Bitmap bitmap){
        this.bitmap = bitmap;

        width = bitmap.getWidth();
        height = bitmap.getHeight();
    }


    public Bitmap getBitmap() {
        return bitmap;
    }

    public void setBitmap(Bitmap bitmap) {
        this.bitmap = bitmap;
    }
}
