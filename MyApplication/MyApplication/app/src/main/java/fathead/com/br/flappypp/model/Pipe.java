package fathead.com.br.flappypp.model;

import android.graphics.Bitmap;

/**
 * Created by Leonardo.Oliveira on 01/06/2016.
 */
public class Pipe {
    private float startX;
    private float startY;
    private Bitmap image;
    private float x, y;

    public Pipe(float startX, float startY, Bitmap image){
        this.startX = startX;
        this.startY = startY;
        this.image = image;
        this.x = startX;
    }

    public Bitmap getImage() {
        return image;
    }

    public void setImage(Bitmap image) {
        this.image = image;
    }

    public float getStartY() {
        return startY;
    }

    public float getStartX() {
        return startX;
    }

    public float getX() {
        return x;
    }

    public void setX(float x) {
        this.x = x;
    }

    public void resetPositionStart(float newPosition){
        x = newPosition;
    }
}
