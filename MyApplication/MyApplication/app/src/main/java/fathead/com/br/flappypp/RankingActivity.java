package fathead.com.br.flappypp;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.KeyEvent;
import android.widget.ListView;

import fathead.com.br.flappypp.Adapter.RankingAdapter;
import fathead.com.br.flappypp.model.Constants;
import fathead.com.br.flappypp.model.Items;

/**
 * Created by Leonardo Souza on 05/06/2016.
 * Classe responsavel pela tela de RANKING.
 */
public class RankingActivity extends Activity {
    //View responsavel por criar uma lista na tela
    ListView listview;
    //Responsavel por customizar e tratar eventos que acontecem na lista
    RankingAdapter rankingAdapter;

    //Metodo iniacial do ciclo de vida do android.
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Passando o layout xml que a activity ira usar.
        setContentView(R.layout.activity_ranking);
        //Declaracao da listview, procurando a por id.
        listview = (ListView) findViewById(R.id.listview);

        loadArray();
        //Declaracao do custom adapter
        rankingAdapter = new RankingAdapter(this, Constants.itemsList);
        //Insercao do adapter a listview
        listview.setAdapter(rankingAdapter);

    }

    //Methodo responsavel por buscar as informacoes no SharedPreferences e criar lista de Items.
    public void loadArray() {
        //Busca o sharedpreference responsavel por armazenar as informacoes do Raking.
        SharedPreferences mSharedPreference1 =   getSharedPreferences("ranking", MODE_PRIVATE);
        // Limpa a lista static de items.
        Constants.itemsList.clear();
        //Pega a quantida de informacoes salva em um campo do sharedpreference
        int size = mSharedPreference1.getInt("Status_size", 0);

        for(int i=0; i<size; i++) {
            //Criacap dos items na lista.
            Constants.itemsList.add(new Items(mSharedPreference1.getString("Name_" + i, null),
                    mSharedPreference1.getString("Score_" + i, null)) );
        }

    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK)) {
            onBackPressed();
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(RankingActivity.this, MenuActivity.class));
        finish();
    }

}
