package fathead.com.br.flappypp.util;

import android.app.Activity;
import android.content.Context;
import android.graphics.BitmapFactory;
import android.util.DisplayMetrics;

import fathead.com.br.flappypp.R;
import fathead.com.br.flappypp.model.Constants;
import fathead.com.br.flappypp.model.Pipe;

/**
 * Created by leonardo on 09/04/16.
 */
public class Utils {

    public static DisplayMetrics getDisplayMetrics(Activity activity){
        DisplayMetrics metrics = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(metrics);
        return metrics;
    }


    public static void createPipesStatic(Context context){
        DisplayMetrics metrics = Utils.getDisplayMetrics((Activity)context);
        Pipe bottom;
        Pipe top;

        top = new Pipe(900, -1100, BitmapFactory.decodeResource(context.getResources(), R.drawable.pipe_i));
        bottom = new Pipe(900, (((metrics.heightPixels * 50) / 100)), BitmapFactory.decodeResource(context.getResources(), R.drawable.pipe));
        Constants.pipes[0][0] = bottom;
        Constants.pipes[0][1] = top;

        top = new Pipe(Constants.pipes[0][0].getStartX() + 600, - Constants.pipes[0][0].getImage().getHeight()-200, BitmapFactory.decodeResource(context.getResources(), R.drawable.pipe_i));
        bottom = new Pipe(Constants.pipes[0][1].getStartX() + 600, (((metrics.heightPixels*50)/100)), BitmapFactory.decodeResource(context.getResources(), R.drawable.pipe));
        Constants.pipes[1][0] = bottom;
        Constants.pipes[1][1] = top;

        top = new Pipe(Constants.pipes[1][0].getStartX() + 700, - Constants.pipes[1][0].getImage().getHeight()+300, BitmapFactory.decodeResource(context.getResources(), R.drawable.pipe_i));
        bottom = new Pipe(Constants.pipes[1][1].getStartX() + 700, (((metrics.heightPixels*50)/100)), BitmapFactory.decodeResource(context.getResources(), R.drawable.pipe));
        Constants.pipes[2][0] = bottom;
        Constants.pipes[2][1] = top;

        top = new Pipe(Constants.pipes[2][0].getStartX() + 800, - Constants.pipes[2][0].getImage().getHeight()+300, BitmapFactory.decodeResource(context.getResources(), R.drawable.pipe_i));
        bottom = new Pipe(Constants.pipes[2][1].getStartX() + 800, (((metrics.heightPixels*50)/100)), BitmapFactory.decodeResource(context.getResources(), R.drawable.pipe));
        Constants.pipes[3][0] = bottom;
        Constants.pipes[3][1] = top;

    }


    public static void createPipes(Context context){
        DisplayMetrics metrics = Utils.getDisplayMetrics((Activity)context);
        Pipe bottom;
        Pipe top;
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 2; j++) {
                if(i == 0) {
                    top = new Pipe(900, -500, BitmapFactory.decodeResource(context.getResources(), R.drawable.pipe_i));
                    bottom = new Pipe(900, (((metrics.heightPixels * 70) / 100)), BitmapFactory.decodeResource(context.getResources(), R.drawable.pipe));
                    Constants.pipes[i][j] = bottom;
                    Constants.pipes[i][j] = top;
                } else {
                    top = new Pipe(Constants.pipes[i-1][0].getStartX() + 600, - Constants.pipes[0][0].getImage().getHeight()+300, BitmapFactory.decodeResource(context.getResources(), R.drawable.pipe_i));
                    bottom = new Pipe(Constants.pipes[i-1][1].getStartX() + 600, (((metrics.heightPixels*50)/100)), BitmapFactory.decodeResource(context.getResources(), R.drawable.pipe));
                    Constants.pipes[i][j] = bottom;
                    Constants.pipes[i][j] = top;
                }
            }
        }
    }

}
