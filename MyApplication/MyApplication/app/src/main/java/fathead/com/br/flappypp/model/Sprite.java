package fathead.com.br.flappypp.model;

import android.graphics.Canvas;
import android.graphics.Rect;

import fathead.com.br.flappypp.GameActivity;

/**
 * Created by leonardo on 02/06/16.
 */
public class Sprite{
    //Position x e y do Sprite
    int x, y;
    //Faz as alteracoes na posicao
    int xSpeed, ySpeed;
    //Altura e largura do Sprite
    int height, width;
    //Variavel hero para obter o Bitmap
    Hero hero;
    //Surface
    GameActivity.MySurfaceView mySurfaceView;
    //Controle qual a o parte da imagem usar
    int currentFrame = 0;

    //Construtor da classe Sprite
    public Sprite(GameActivity.MySurfaceView mySurfaceView, Hero hero) {
        this.hero = hero;
        this.mySurfaceView = mySurfaceView;

        //Pega a altura do sprite do bitmap do hero
        height = hero.getBitmap().getHeight();
        //Tamnho de cada imagem do spite, dividido por 3 pois e a quantidade de imagens por linha
        width = hero.getBitmap().getWidth()/3;
        //Inicializa as posicao e velocidades
        x = y = 0;
        xSpeed = 0;
        ySpeed = 15;
    }

    //Atualiza a velocidade em Y
    public void setYSpeed(int ySpeed){
        this.ySpeed = ySpeed;
    }

    //Desenha o sprite na surface.
    public void onDraw(Canvas canvas) {
        update();
        //Em que posicao da imagem em x que sera recortado
        int srcX = currentFrame * width;
        Rect src = new Rect(srcX, 0, srcX + width, height);
        Rect dst = new Rect(x, y, x+width, y+height);
        canvas.drawBitmap(hero.getBitmap(),src, dst, null);
    }

    //Faz o update nas variaves de posicao, currentFrame e faz a chamada do metodo game over
    private void update() {

        if(y > mySurfaceView.getHeight() - height - ySpeed){
            mySurfaceView.gameOver();
        }

        if(y + ySpeed == 0){
            mySurfaceView.gameOver();
        }

        //Tempo de atualizacao da Thread
        try {
            Thread.sleep(20);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        x += xSpeed;
        y += ySpeed;
        currentFrame = ++currentFrame % 3;
    }
}
