package fathead.com.br.flappypp;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import fathead.com.br.flappypp.model.AudioManager;

/**
 * Created by leonardo on 30/04/16.
 * Classe responsavel pelo menu do game.
 */

public class MenuActivity extends Activity implements View.OnClickListener{
    //Intent e classe que e ultilizada pela trasicao de activities e troca de informacao entre elas
    Intent intent;
    //Botoes de referencia para outras activities do projeto.
    Button playButton, rankingButton;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        createViews();
    }

    //Metodo responsavel por instaciar as view presente na tela, as buscando por ID.
    private void createViews(){

        playButton = (Button) findViewById(R.id.play_button);
        //Sentando o evento de click no botao.
        playButton.setOnClickListener(this);

        rankingButton = (Button) findViewById(R.id.ranking_button);
        //Sentando o evento de click no botao.
        rankingButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.play_button:
                //Cria e faz a transicao para a GameActivity
                intent = new Intent(this, GameActivity.class);
                this.startActivity(intent);
                break;
            case R.id.ranking_button:
                //Cria e faz a transicao para a RankingActivity
                intent = new Intent(this, RankingActivity.class);
                this.startActivity(intent);
                break;
            default:
                break;
        }
    }
}
