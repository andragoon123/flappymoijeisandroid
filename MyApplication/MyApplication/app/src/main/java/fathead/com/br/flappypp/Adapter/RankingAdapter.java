package fathead.com.br.flappypp.Adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import fathead.com.br.flappypp.R;
import fathead.com.br.flappypp.model.Constants;
import fathead.com.br.flappypp.model.Items;

/**
 * Created by asus-pc on 10/06/2016.
 */
public class RankingAdapter extends BaseAdapter {

    Context context;
    List<Items> data;
    private static LayoutInflater inflater = null;

    public RankingAdapter(Context context, List<Items> data) {
        // TODO Auto-generated constructor stub
        this.context = context;
        this.data = data;
        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        View vi = convertView;
        ViewHolder holder;

        if (vi == null) {
            vi = inflater.inflate(R.layout.custom_row, null);
            holder = new ViewHolder();

            holder.name = (TextView) vi.findViewById(R.id.ranking_name);
            holder.score = (TextView) vi.findViewById(R.id.ranking_score);

        } else {
            holder = (ViewHolder) vi.getTag();
        }

        holder.name.setText(data.get(position).getName());
        holder.score.setText("" + Integer.parseInt(data.get(position).getScore())/40);

        vi.setTag(holder);
        return vi;
    }


    class ViewHolder{
        TextView name, score;
    }
}
